import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  constructor() { }

  customer:Customer = new Customer();

  ngOnInit(): void {
  }

  onSubmit(){
    alert(JSON.stringify(this.customer))
  }

}
